extends Node2D

enum Distance {
	LONG,
	SHORT
}

export(Distance) var distance = Distance.SHORT

onready var animation = $AnimationTree
onready var animation_state = animation.get("parameters/playback")

func _ready():
	animation.active = true

func attack():
	$Sprite/Hitbox/CollisionShape2D.set_deferred("disabled", false)
	animation_state.travel("attack")

func disable_collision():
	$Sprite/Hitbox/CollisionShape2D.set_deferred("disabled", true)

func sprite_direction(direction):
	direction = Vector2(direction,0)
	animation.set("parameters/idle/blend_position", direction)
	animation.set("parameters/attack/blend_position", direction)
