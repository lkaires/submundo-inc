extends Control

var hearts = []

func _ready():
	for x in get_children():
		hearts.append(x)
	heart_update()

func health_update():
	var count = (Global.player_hearts*Global.HEALTH_PER_HEART)-Global.player_health
	
	for x in range(-Global.player_hearts+1,1):
		if count > Global.HEALTH_PER_HEART:
			hearts[-x].frame = 0
			count -= Global.HEALTH_PER_HEART
		elif count > 0:
			count = Global.HEALTH_PER_HEART-count
			hearts[-x].frame = count
			count = 0
		else:
			hearts[-x].frame = Global.HEALTH_PER_HEART

func heart_update():
	for x in range(0,hearts.size()):
		if x < Global.player_hearts:
			hearts[x].visible = true
		else:
			hearts[x].visible = false
	
	health_update()
