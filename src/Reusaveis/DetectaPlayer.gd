extends Area2D

enum Tamanho {
	PEQUENO,
	MEDIO,
	GRANDE
}
export(Tamanho) var tamanho = Tamanho.PEQUENO

signal chase

var player = null
onready var area = $CollisionShape2D.shape

func _ready():
	match tamanho:
		Tamanho.PEQUENO:
			area.set_radius(80)
		Tamanho.MEDIO:
			pass
		Tamanho.GRANDE:
			pass

func _on_DetectaPlayer_body_entered(body):
	player = body
	emit_signal("chase")

func _on_DetectaPlayer_body_exited(body):
	player = null

func chase_player():
	if player == null:
		return null
	return player.get_position()
