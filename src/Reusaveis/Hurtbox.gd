extends Area2D

export(Global.Category) var category = Global.Category.PLAYER
onready var collision_shape = $Player

func _ready():
	match category:
		Global.Category.PEQUENO:
			collision_shape = $Pequeno
		Global.Category.MEDIO:
			collision_shape = $Medio
		Global.Category.GRANDE:
			collision_shape = $Grande
	
	collision_shape.set_deferred("disabled", false)
	collision_shape.visible = true
	
	if category == Global.Category.PLAYER:
		set_collision_mask_bit(6,true)
		set_collision_mask_bit(5,false)
