extends KinematicBody2D
class_name Enemy

signal enemy_death

enum {
	IDLE,
	CHASE
}

var category = Global.Category.PEQUENO
var state = IDLE
var health = Global.HEALTH_PER_HEART*Global.player_level

var direction = 0
var velocity = Vector2.ZERO
var attack_time = 5

onready var animated_sprite = $AnimatedSprite
onready var detecta = $DetectaPlayer
onready var attack_timer = $Ataque
onready var hitbox = $Hitbox/CollisionShape2D

func _ready():
	animated_sprite.set_animation(str(0))
	animated_sprite.playing = true
	attack_time = int(5/Global.player_level)
	if attack_time < 1:
		attack_time = 1
	$Hitbox.connect("area_entered",self,"next_attack")
	attack_timer.connect("timeout",self,"attack")
	attack_timer.one_shot = true

func _on_DetectaPlayer_chase():
	state = CHASE

func _on_Hurtbox_area_entered(area):
	health -= Global.player_strenght
	if health <= 0:
		emit_signal("enemy_death",self)

func next_attack(area):
	attack_timer.wait_time = attack_time
	attack_timer.start()
	hitbox.set_deferred("disabled", true)

func attack():
	hitbox.set_deferred("disabled", false)
