extends Enemy

const MAX_SPEED = 20
const GRAVITY = 40

var jump = false

func _physics_process(delta):
	if velocity.y == 0:
		jump = true
	
	match state:
		IDLE:
			if jump:
				direction = Global.random.randi_range(-1,1)
		CHASE:
			_chase()
	
	_animation()
	_move(delta)

func _move(delta):
	velocity.y += GRAVITY*delta
	if jump:
		velocity.y = -MAX_SPEED*2
		jump = false
	
	velocity.x = direction*MAX_SPEED
	
	velocity = move_and_slide(velocity, Vector2(0, -1))

func _chase():
	var player_position = detecta.chase_player()
	if player_position == null:
		state = IDLE
		return
	player_position = (player_position-position).normalized()
	direction = player_position.x

func _animation():
	if velocity.y <= 0:
		$AnimatedSprite.frame = 0
	else:
		$AnimatedSprite.frame = 1
